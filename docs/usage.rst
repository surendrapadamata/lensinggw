Quickstart
----------

This section illustrates ``lensingGW``'s core functionalities through
worked-out examples. They demonstrate the solver usage and the simulation of
the lensed and unlensed gravitational signals.

The lens model is specified through a list of lens profile names and a list of related parameters. It is handled through a `forked version of Lenstronomy <https://github.com/gipagano/lenstronomy>`_ (original version `here <https://github.com/sibirrer/lenstronomy>`_): any lens
profile implemented therein can be used.

Ready-to-use ``python`` scripts implementing the following examples are provided along with each subsection and at the end of the section.

Solve the lens model 
~~~~~~~~~~~~~~~~~~~~
.. include:: usage_radians.rst
.. include:: usage_au.rst
.. include:: usage_multi_component.rst
.. include:: usage_only_macro.rst

Simulate gravitational waves 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Configuration files**

If gravitational-wave (GW) signals are requested, a configuration file specifying the binary parameters and the waveform settings
has to be provided to ``lensingGW``. A `template configuration file <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/ini_files/waveform_config_template.ini>`_
with explanations of each entry and a `ready-to-use configuration file <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/ini_files/waveform_config.ini>`_
are provided in **examples/ini_files** on GitLab.

**Note on angles**

| GWs from compact binaries depend on a set of angles. In addition to the binary right ascension and declination,
  the waveform depends on the inclination of the system, the polarization angle and on the phase of coalescence.
| The standard choice in gravitational-wave astronomy is to indicate such angles in radians. To avoid confusion, the same
  choice has been adopted within ``lensingGW``. This only affects the angles specified in the waveform configuration file:
  although any routine of ``lensingGW`` supports the definition of arbitrary units for the source coordinates,
  the source right ascension and declination in the configuration file must by expressed in radians. 



**Interferometers' noise curves**

Power spectral densities (PSDs) have to be provided for each interferometer in which the waveform is requested.
`Advanced LIGO <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/psds/LIGO-P1200087-v18-aLIGO_DESIGN_psd.dat>`_ and `Advanced Virgo <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/psds/LIGO-P1200087-v18-AdV_DESIGN_psd.dat>`_
design PSDs released by the LIGO/Virgo Collaborations are provided in
**examples/psds** on GitLab.

.. include:: usage_unlensedGW.rst
.. include:: usage_lensedGW.rst

User-defined cosmology
~~~~~~~~~~~~~~~~~~~~~~
.. include:: usage_cosmology.rst


Examples
~~~~~~~~
Ready-to-use ``python`` scripts implementing the previous examples:

+ `Example 1 - Solve the lens model (radians) <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/binary_point_mass.py>`_
+ `Example 2 - Solve the lens model (arbitrary units) <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/binary_point_mass_scaled_units.py>`_
+ `Examples 3 & 4 - Solve the lens model (multi-component macromodel and only macromodel) <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/binary_point_mass_only_macromodel.py>`_
+ `Example 5 - Get the unlensed gravitational waves <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/unlensed_waveform.py>`_
+ `Example 6 - Get the lensed gravitational waves <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/lensed_waveform.py>`_